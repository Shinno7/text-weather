<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('city')->insert([
            [
                'name' => 'Омск',
                'lat' => '54.983334',
                'lon' => '73.366669',
            ],
            [
                'name' => 'Не Омск',
                'lat' => '55.751244',
                'lon' => '37.618423',
            ],
            [
                'name' => 'Совсем не Омск',
                'lat' => '40.730610',
                'lon' => '-73.935242',
            ]
        ]);
    }
}
