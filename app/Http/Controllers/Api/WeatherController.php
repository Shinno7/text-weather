<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\City;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App;

class WeatherController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function getWeather(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required|string|max:255',
            'lon' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            $city=$this->city;
        } else {
            //find city name by lat,lon or create api for get any city use zip code and country code
            $city=City::getByLonLat($request->lon,$request->lat);
            if(!$city){
                $this->city->name='';
                $this->city->lon=$request->lon;
                $this->city->lat=$request->lat;
                $city=$this->city;
            }
        }
        $url=self::YANDEX_WHETHER_URL.'?lat='.$city->lat.'&lon='.$city->lon.'&lang=ru_RU';
        $json=self::requestYandex($url);
        $array=json_decode($json);
        if(!$array){
            return $this->sendError('Server unavailable.', '',400);
        }
      //  dd($array);
        $answer=WeatherController::prepareAnswer($array,$city);
        return $this->sendResponse($answer, 'Done.');
    }

    public static function prepareAnswer($array,$city){

        $answer=array();
        $answer['city']=$city->name;
        $answer['temp']=$array->fact->temp;
        $answer['icon']='https://yastatic.net/weather/i/icons/funky/dark/'.$array->fact->icon.'.svg';
        $answer['condition']=Lang::get('weather.'.$array->fact->condition);
        $answer['wind_speed']=$array->fact->wind_speed . ' м/с';
        $answer['pressure_mm']=$array->fact->pressure_mm . ' мм рт. ст.';
        $answer['humidity']=$array->fact->humidity . ' %';
        $answer['prec_prob']=$array->forecast->parts[0]->prec_prob . ' %';
        return $answer;
    }

    public function getCities(){

        $cities=City::getAll();
        $answer=array();
        foreach($cities as $key=>$city){
            $answer[$key]['id']=$city->id;
            $answer[$key]['name']=$city->name;
            $answer[$key]['lat']=$city->lat;
            $answer[$key]['lon']=$city->lon;
        }
        return $this->sendResponse($answer, 'Done.');
    }

    public function requestYandex($url){

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            "X-Yandex-API-Key: ".getenv('YANDEX_KEY')."",
            "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $json = curl_exec($curl);
        curl_close($curl);
        return $json;
    }
}
