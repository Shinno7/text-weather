<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class City extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function getAll(){

        return City::get();
    }

    public static function getByLonLat($lon,$lat){

        $city=City::where([['lon',$lon],['lat',$lat]])
            ->first();
        return $city;
    }
}
