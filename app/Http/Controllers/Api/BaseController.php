<?php

namespace App\Http\Controllers\Api;

use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

class BaseController extends Controller
{
    const DEFAULT_CITY='Омск';
    const DEFAULT_LAT='54.983334';
    const DEFAULT_LON='73.366669';
    const YANDEX_WHETHER_URL='https://api.weather.yandex.ru/v2/informers';

    public function __construct(Request $request)
    {
        $this->city=(object)array();
        $this->city->name = self::DEFAULT_CITY;
        $this->city->lat = self::DEFAULT_LAT;
        $this->city->lon = self::DEFAULT_LON;
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'pageData'  => $result,
            'message' => $message,
        ];
        Log::info('sendResponse 200 : '.date("Y-m-d H:i:s") . ' '.json_encode($response));
        return response()->json($response, 200);
    }
    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        Log::info('sendResponse 400 : '.date("Y-m-d H:i:s") . ' '.json_encode($response));
        return response()->json($response, $code);
    }
}
